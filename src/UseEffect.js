import React, {useState, useEffect} from 'react'

function UseEffect() {
    const [click, setClick] = useState(0);
    const [name, setName] = useState("");

    useEffect(() => {
        document.title = `you clicked ${click} times`;
        console.log('updated', {click});
    }, [click]);
  
  return (
      <>
       <h1>UseEffect</h1>
       <input type="text" value ={name} onChange={e => setName(e.target.value)}/>
       <button onClick={() => {setClick(click + 1)}}>clicked {click} times</button>
      </>
  )
}

export default UseEffect