import React, {useState} from 'react'

function CounterArr() {
  const [items, setItems] = useState([]);
  const addItem = () => {
    setItems([...items, {
      id: items.length,
      value: Math.floor(Math.random() * 10 ) + 1
    }])
  }
  return (
    <>
      <h1>array use state hook</h1>
      <button onClick={addItem}> add Item </button>
      <ul>
        {items.map(item => <li key={item.id}>{item.value}</li>)}
      </ul>
    </>
  )
}

export default CounterArr