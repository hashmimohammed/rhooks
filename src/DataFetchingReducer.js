import React, { useState, useEffect, useReducer } from 'react'
import axios from 'axios';

const initialState = {
    loading: true,
    error: '',
    post: {}
}

const reducer = (state, action) => {
    switch(action.type){
        case 'loading':
            return {...state, loading: false}
        case 'error':
            return {...state, error: action.payload, loading: false}
        case 'success':
            return {...state, post: action.payload, loading: false}
        default:
            return state;
    }
}

function DataFetchingReducer() {
    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        axios.get('https://jsonplaceholder.typicode.com/posts/1')
        .then(res => {
            dispatch({type: 'loading'});
            dispatch({type: 'success', payload: res.data});
        })
        .catch(err => {
            dispatch({type: 'loading'});
            dispatch({type: 'error', payload: 'some thing went wrong'});
        })
    }, [])

  return (
    <div>
        <h1>{state.loading ? 'loading' : state.post.title}</h1>

    </div>
  )
}

export default DataFetchingReducer