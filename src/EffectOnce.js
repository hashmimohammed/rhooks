import React, {useState, useEffect} from 'react'
function EffectOnce() {
    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    const logMousePosition = (e) => {
        console.log('updating the position');
        setX(e.clientX);
        setY(e.clientY);
    }


    useEffect(() => {
        console.log('use effect called');
        window.addEventListener('mousemove', logMousePosition);

        return () => {
            window.removeEventListener('mousemove', logMousePosition);
        }
    }, []);

  
    return (
        <>
            <h1>EffectOnce</h1>
            <h1>X: {x} - Y: {y}</h1>
        </>
    )
}

export default EffectOnce