import React, {useState} from 'react'

function Counter() {

    const [name, setName] = useState({firstName: 'mohammed', lastName: 'Hashmi'});

    return (
        <div>
            <h1>counter</h1>
            <input value={name.firstName} onChange={e => setName({...name, firstName: e.target.value})}/>
            <input value={name.lastName} onChange={e => setName({...name, lastName: e.target.value})}/>
            <h2>{name.firstName}</h2>
            <h2>{name.lastName}</h2>
        </div>
    )
}

export default Counter