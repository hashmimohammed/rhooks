import React, { useState } from 'react'
import EffectOnce from './EffectOnce';

function MouseContainer() {
    const [display, setDisplay] = useState(true)
    
    const toggleDisplay = () => {
        setDisplay(!display);
    }

  return (
      <>
        <button onClick={toggleDisplay}>Toggle Display</button>
        {display && <EffectOnce/>}
      </>
  )
}

export default MouseContainer