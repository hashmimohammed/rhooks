import React, {useContext} from 'react'

import {CountContext} from './App'

function ContextComponentA() {
  const countContext = useContext(CountContext);
  console.log(countContext);
  return (
    <div>
        <h1>ContextComponentA - {countContext.countState}</h1>
        <button onClick={() => countContext.countDispatch('increment')}>increment</button>
        <button onClick={() => countContext.countDispatch('decrement')}>Decrement</button>
        <button onClick={() => countContext.countDispatch('reset')}>Reset</button>
    </div>
  )
}

export default ContextComponentA