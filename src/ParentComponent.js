import React, {useState, useCallback} from 'react'
import Count from './Count'
import Button from './Button'

function ParentComponent() {
    const [age, setAge] = useState(25);
    const [salary, setSalary] = useState(50000);

    const incAge = useCallback(() => {
        setAge(age + 1);
    }, [age])

    const incSal = useCallback(() => {
        setSalary(salary + 1000)
    }, [salary])

    return (
        <div>

            <Count text="Age" count={age}/>
            <Button handleClick={incAge}>Increment Age</Button>
            <Count text="salary" count={salary}/>
            <Button handleClick={incSal}>Increment Salary</Button>

        </div>
    )
}

export default ParentComponent