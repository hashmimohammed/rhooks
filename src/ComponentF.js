import React, {useContext} from 'react'
import {UserContext, ChannelContext} from './App'

function ComponentF() {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);
  return (
    <div>
        <h1>User Context value {user} and channel context is {channel}</h1>
    </div>
  )
}

export default ComponentF